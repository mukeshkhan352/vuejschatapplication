var getTemp = `
    <div style="width: 320px;border: 2px solid;">
        <h1>{{companyName}}</h1>
        <hr/>
        <template v-for="userDet in userDetails" :key="userDet.id">
            <p @click="$emit('namePass', userDet)" v-if=getChatUserName(userDet)>{{chatUser}}</p>
        </template>
    </div>
`

export default {
    name: 'side',
    data(){
        return {
            message: 'Reach App',
            chatUser: ''   
        }
    },
    template: getTemp,
    props: ['userDetails', 'companyName'],
    emits: ['namePass'],
    methods: {
        getChatUserName(val){
            if(val.confirmed == "1"){
                return this.chatUser = val.name;
            }
        }
    }
}