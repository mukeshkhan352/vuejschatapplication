var getTemp = `
    <div>
        <div>
        <nav class="navbar navbar-expand-lg bg-dark">
            <div class="container-fluid">
                <a class="navbar-brand">{{chatUserDetails.chatEmpDet.name}}</a>
            </div>
        </nav>
        </div>
    </div>
`

export default {
    name: 'chatBodyHeader',
    data(){
        return {
            message: 'Sub Chat'
        }
    },
    template: getTemp,
    inject: ['chatUserDetails'],
    emits: ['passProjectBeta']
}

{/* <div v-if="userName.id.projectBeta">
                    
                    <button v-on:click="$emit('passProjectBeta', false)">Project Beta</button>
                </div> */}

                