import chatBodyHeader from './chatBodyHeader.js'

var getTemp = `
    <div style="flex: 1; width: calc(100vw - 320px); border: 2px solid; position: relative;">
        <chatBodyHeader />
    </div>
`

export default {
    components: {chatBodyHeader},
    name: 'mainChatBody',
    data(){
        return {
            message: 'Body Chat Controller'
        }
    },
    template : getTemp
}