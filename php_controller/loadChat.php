<?php


$host = "localhost";    /* Host name */
$user = "root";         /* User */
$password = "";         /* Password */
$dbname = "halsimplify";   /* Database name */

// Create connection
$con = mysqli_connect($host, $user, $password,$dbname);

// Check connection
if (!$con) {
    die("Connection failed: " . mysqli_connect_error());
}

// $condition  = "1";
// if(isset($_GET['userid'])){
// 	$userid = mysqli_real_escape_string($con,trim($_GET['userid']));
//
// 	$condition  = " id=".mysqli_real_escape_string($con,$_GET['userid']);
// }
$userData = mysqli_query($con,"select * from users");

$response = array();

while($row = mysqli_fetch_assoc($userData)){

    $response[] = $row;
}

echo json_encode($response);
exit;
