// import test from './header/test';
import sidechat from './components/sidebar_chat/sideChat.js'
import mainChatBody from './components/chatbody/mainChatBody.js'

export default {
    name: 'Root',
    components: {sidechat, mainChatBody},
    data() {
        return {
            blogHeading: 'Chat Application',
            userDetails: [],
            companyName: 'HAL SIMPLIFY',
            chatEmployeeDetails: {name: 'Please Select Chat'}            
        };
    },
    
    template: `
      <div class="d-flex"> 
        <sidechat @namePass="getUserChatDetails" :userDetails="userDetails" :companyName="companyName"/>

        <mainChatBody />
      </div>
    `,
    methods:{
      fetchUserDetails(){
        var _this = this;
        const config = {
          headers: {
            "responseType": 'text'
          }
        };
        axios.get('./php_controller/loadChat.php', config)
          .then(function (response) {
            return _this.userDetails = response.data;
          })
          .catch(function (error) {
            alert(error);
				});
      },
      getUserChatDetails(clickChatDetails){
        console.log(clickChatDetails);
        this.chatEmployeeDetails = clickChatDetails;
      }
      
    },
    computed(){

    },
    created(){
      this.fetchUserDetails();
    },
    provide(){
      const chatUserDetails = {};
      Object.defineProperty(chatUserDetails, 'chatEmpDet', {
        enumerable: true,
        get: () => this.chatEmployeeDetails
      })

      return {
        chatUserDetails
      }
    }
};

// v-for="blog in blogName" :key="blog.id" :title="blog.name"